#fill in server names here:
server1=myk8stest-1
server2=myk8stest-2
server3=myk8stest-3
server1IP=192.168.100.51
server2IP=192.168.100.52
server3IP=192.168.100.53
#remember to also change interace if it is different than ens3
#this will go to keepalived/vips.yml
myk8sVIP1=192.168.100.61
myk8sVIP2=192.168.100.62
myk8sVIP3=192.168.100.63

git clone https://github.com/kubernetes-incubator/kubespray
git clone https://github.com/UnderGreen/ansible-prometheus-node-exporter.git prometheus/roles/UnderGreen.prometheus-node-exporter   
git clone https://github.com/UnderGreen/ansible-prometheus-exporters-common.git prometheus/roles/UnderGreen.prometheus-exporters-common 

ssh $server1 apt install python -y
ssh $server2 apt install python -y
ssh $server3 apt install python -y

#prepare all config files
mkdir .myk8s_config
cp -Rf myk8s_config/* .myk8s_config/
find .myk8s_config/ -type f | xargs -i sed -i "s/myk8stestserver-1/$server1/" {}
find .myk8s_config/ -type f | xargs -i sed -i "s/myk8stestserver-2/$server2/" {}
find .myk8s_config/ -type f | xargs -i sed -i "s/myk8stestserver-3/$server3/" {}
find .myk8s_config/ -type f | xargs -i sed -i "s/myk8sServerIP-1/$server1IP/" {}
find .myk8s_config/ -type f | xargs -i sed -i "s/myk8sServerIP-2/$server2IP/" {}
find .myk8s_config/ -type f | xargs -i sed -i "s/myk8sServerIP-3/$server3IP/" {}
sed -i "s/myk8sVIP-1/$myk8sVIP1/" .myk8s_config/keepalived/vips.yml
sed -i "s/myk8sVIP-2/$myk8sVIP2/" .myk8s_config/keepalived/vips.yml
sed -i "s/myk8sVIP-3/$myk8sVIP3/" .myk8s_config/keepalived/vips.yml
cp -Rf .myk8s_config/* ./

#kubespray deploys k8s
ansible-playbook -i kubespray/inventory/sample/hosts.ini kubespray/cluster.yml 
scp -r root@$server1:/root/.kube ~/

#gk deploys heketi and gluster
#this also adds a default storageclass
ansible-playbook -i gk/ansible-nodeconfig/myk8s_inventory gk/ansible-nodeconfig/prepare_nodes.yml
cd gk
./gk-deploy -gy
cd ..
sed -i "s/heketiClusterIP/$(kubectl get svc heketi | grep heketi | awk '{ print $3 }')/" gk/kubernetes-storageclass.yml
kubectl apply -f gk/kubernetes-storageclass.yml

# deploy node exporter and prometheus
ansible-playbook -i prometheus/myk8s_inventory prometheus/deploy-node-exporter.yml
kubectl apply -f prometheus/kubernetes/monitoring-namespace.yml
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /tmp/tls.key -out /tmp/tls.crt -subj "/CN=grafana.vip.myk8s.test"
kubectl create secret tls grafana-secret --key /tmp/tls.key --cert /tmp/tls.crt -n monitoring 
kubectl apply -f prometheus/kubernetes/prometheus.yml
kubectl apply -f prometheus/kubernetes/grafana.yml
echo Getting pod name.
grafanaPod="placeholder"
while [[ $grafanaPod != grafana* ]]
do
	grafanaPod=$(kubectl get pod -n monitoring --selector app=grafana | grep grafana | awk '{ print $1 }')
        sleep 5;
done
echo Found $grafanaPod
testPod="placeholder"
echo Waiting for $grafanaPod to be available ...
while [[ $testPod != $grafanaPod ]]
do
        testPod=$(kubectl exec -n monitoring $grafanaPod /bin/hostname)
        sleep 5;
done
echo "Copying files to $grafanaPod (to the persisten volume)"
kubectl cp prometheus/kubernetes/configs/grafana monitoring/$grafanaPod:/var/lib/
kubectl delete pod -n monitoring $grafanaPod

#add the VIPs
ansible-playbook -i keepalived/myk8s_inventory keepalived/deploy-keepalived.yml


#add the final stuff
echo "Adding some final stuff..."
kubectl delete svc -n kube-system kubernetes-dashboard
kubectl apply -f extra-k8s-stuff/admin-user.yaml
kubectl apply -f extra-k8s-stuff/dashboard-svc.yaml
echo Here are the ports of Grafana and for the dashboard:
kubectl get svc --all-namespaces | grep -e grafana -e dashboard
echo The token for the dashboard is:
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') | grep token | awk '{ print $2 }'
