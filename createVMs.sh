server1=myk8stest-1
server2=myk8stest-2
server3=myk8stest-3

./cloneVM-myk8snet.sh myk8s-template $server1 51
./cloneVM-myk8snet.sh myk8s-template $server2 52
./cloneVM-myk8snet.sh myk8s-template $server3 53

qemu-img create -f qcow2 /var/lib/libvirt/images/$server1-1.qcow2 60G
qemu-img create -f qcow2 /var/lib/libvirt/images/$server2-1.qcow2 60G
qemu-img create -f qcow2 /var/lib/libvirt/images/$server3-1.qcow2 60G

virsh attach-disk $server1 --driver qemu --subdriver qcow2 --source /var/lib/libvirt/images/$server1\-1.qcow2 --target vdb --persistent
virsh attach-disk $server2 --driver qemu --subdriver qcow2 --source /var/lib/libvirt/images/$server2\-1.qcow2 --target vdb --persistent
virsh attach-disk $server3 --driver qemu --subdriver qcow2 --source /var/lib/libvirt/images/$server3\-1.qcow2 --target vdb --persistent
