# This script can be used to clone VMs fast
# It will automatically assign IP and hostname to the VM
# Make sure you have ssh keys within the template, for your user and for root

# Further upgrades to the script are required:
#	- support for centos, redhat, archlinux
#	- checks to stop script if something failed
#	- support for multiple subnets with extra parameter
# Feel free to commit!

if [ $# -ne 3 ]
  then
    echo "Usage: ./cloneVM-ubuntu.sh template-name new-vm-name ip"
    echo "you only need to enter the last number within the IP, without the subnet"
    echo "example: ./cloneVM-myk8snet.sh myk8s-template testvm 15"
    exit 1
fi

templateName=$1
requiredVMName=$2
requiredIP=$3

echo creating $requiredVMName from $templateName

sudo virt-clone --original $templateName --name $requiredVMName --file /var/lib/libvirt/images/$requiredVMName.qcow2
sudo chown root:kvm /var/lib/libvirt/images/$requiredVMName.qcow2

sudo virsh start $requiredVMName

echo "Waiting for instance to boot ..."

newVMIP="placeholder"

while [[ $newVMIP != 192.168.100.* ]]
do
        newVMIP=$(cat /proc/net/arp | grep $(sudo virsh dumpxml $requiredVMName | grep -oE "([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})") | cut -d" " -f1)
        sleep 5;
done



echo "
127.0.0.1	localhost
127.0.1.1	$requiredVMName $requiredVMName.myk8s.test

" | ssh -o StrictHostKeyChecking=no root@$newVMIP "cat > /etc/hosts"


echo "$requiredVMName.myk8s.test" | ssh root@$newVMIP "cat > /etc/hostname"

ssh root@$newVMIP mkdir -p /etc/resolvconf/resolv.conf.d
echo "nameserver 192.168.100.10" | ssh root@$newVMIP "cat > /etc/resolvconf/resolv.conf.d/base"
# we asume the device name is ens3 since the script is for qemu
echo "
source /etc/network/interfaces.d/*
auto lo
iface lo inet loopback
auto ens3
iface ens3 inet static
    address 192.168.100.$requiredIP
    netmask 255.255.255.0
    gateway 192.168.100.1

" | ssh root@$newVMIP "cat > /etc/network/interfaces"

ssh root@$newVMIP reboot
echo done
