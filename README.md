# Bash+Ansible deployment of K8S+Heketi+Prometheus+stuff

This project was written as a test of a fully automated deployment of kubernetes. 
`createVMs.sh` and `cloneVM-myk8snet.sh` are used to create VMs on top of KVM, for easy testing.
The folder `myk8s_config` contains all the config files that get injected into all other folder.
`deploy.sh` calls multiple elements in apropriate order, so that everything gets deployed:
1. kubespray ansible playbooks that deploy k8s
2. gk-deploy, a bash script that deploys gluster and heketi
3. ansible playbook to deploy node-exporter to nodes
4. k8s manifest filest to deploy prometheus, grafana for monitoring
5. keepalived ansible playbook, so that the nodes have VIPs
6. final stuff including access to dashboard for user
